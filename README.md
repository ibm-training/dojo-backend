# DOJO - Backend

| Data | Autor | Comentários | Versão |
| --- | --- | --- | --- |
| 18/08/2019 | Paulo Uechi | Inicio do desenvolvimento deste documento | 1.0 |  

Este dojo visa o treinamento básico para novos programadores backend focando no desenvolvimento completo de uma API Rest. Abaixo segue todas as etapas demonstradas neste documento.

**Plano de Treinamento**

1. Sobre o Java 
2. Ferramenta IDE (STS e IntelliJ)
3. Maven
4. Git
5. Introdução ao DevOps (CD CI)
6. Cloud Heroku / IBMCloud
    - https://developer.ibm.com/articles/use-spring-with-ibm-cloud-and-ibm-software/
    - https://developer.ibm.com/articles/create-and-deploy-a-spring-microservice-in-minutes/
7. Base de dados
    7.1. Document Based ( MongoDB)
    7.2. BDR - MySql
8. Restful - Métodos, response codes e melhores práticas.




