# DOJO - Backend

*DOJO IBM - Treinamento básico para programadores backend*

**1. Ferramenta IDE (STS e IntelliJ)**

**Resumo:**

IDE, do inglês Integrated Development Environment ou Ambiente de Desenvolvimento Integrado, é um programa de computador que reúne características e ferramentas de apoio ao desenvolvimento de software com o objetivo de agilizar este processo.[1][2]

Geralmente os IDEs facilitam a técnica de RAD (de Rapid Application Development, ou "Desenvolvimento Rápido de Aplicativos"), que visa a maior produtividade dos desenvolvedores.

As características e ferramentas mais comuns encontradas nos IDEs são:

Editor - edita o código-fonte do programa escrito na(s) linguagem(ns) suportada(s) pela IDE;
Compilador (compiler) - compila o código-fonte do programa, editado em uma linguagem específica e a transforma em linguagem de máquina;
Linker - liga (linka) os vários "pedaços" de código-fonte, compilados em linguagem de máquina, em um programa executável que pode ser executado em um computador ou outro dispositivo computacional;
Depurador (debugger) - auxilia no processo de encontrar e corrigir defeitos no código-fonte do programa, na tentativa de aprimorar a qualidade de software;
Modelagem (modeling) - criação do modelo de classes, objetos, interfaces, associações e interações dos artefatos envolvidos no software com o objetivo de solucionar as necessidades-alvo do software final;
Geração de código - característica mais explorada em Ferramentas CASE, a geração de código também é encontrada em IDEs, contudo com um escopo mais direcionado a templates de código comumente utilizados para solucionar problemas rotineiros. Todavia, em conjunto com ferramentas de modelagem, a geração pode gerar praticamente todo o código-fonte do programa com base no modelo proposto, tornando muito mais rápido o processo de desenvolvimento e distribuição do software;
Distribuição (deploy) - auxilia no processo de criação do instalador do software, ou outra forma de distribuição, seja discos ou via internet;
Testes Automatizados (automated tests) - realiza testes no software de forma automatizada, com base em scripts ou programas de testes previamente especificados, gerando um relatório, assim auxiliando na análise do impacto das alterações no código-fonte. Ferramentas deste tipo mais comuns no mercado são chamadas robôs de testes;
Refatoração (refactoring) - consiste na melhoria constante do código-fonte do software, seja na construção de código mais otimizado, mais limpo e/ou com melhor entendimento pelos envolvidos no desenvolvimento do software. A refatoração, em conjunto com os testes automatizados, é uma poderosa ferramenta no processo de erradicação de "bugs", tendo em vista que os testes "garantem" o mesmo comportamento externo do software ou da característica sendo reconstruída.

**Prática:**

*STS*

Spring Tool Suite for Eclipse

O Spring Tool Suite é uma Ferramenta de ambiente de desenvolvimento baseado na distribução do Eclipse que é Opensource ou seja, livre para desenvolvimento e customização, no caso, aplicações Spring. O STS fornece um ambiente pronto para implementar, depurar, executar e implantar seus codigos em Spring, incluindo as integrações Pivotal TC Server, Pivotal Cloud Foundry, Git, Maven, AspectJ, que integram todos lançamentos do Eclipse.

Instalação


Para fazer o download do Spring STS basta acessar o site no link https://spring.io/tools3/sts/all e seguir os procedimentos após o download para instalação.

O que é o Spring Boot
Este tutorial usa o Spring Boot para criar um aplicativo da web simples com execução no servidor Tomcat integrado.

O Spring Boot é a solução da Spring para criar aplicativos independentes que são fáceis de criar e executar. Ele usa a plataforma Spring pré-configurada e bibliotecas de terceiros para que você possa começar em poucos minutos. A maioria dos aplicativos Spring Boot precisa de uma configuração de Spring muito pequena. Este exemplo específico não precisa de nenhuma configuração.

Recursos do Spring Boot:

. Crie aplicativos Spring independentes
. Incorpore Tomcat ou Jetty diretamente (não é necessário implantar arquivos WAR)
. Forneça o arquivo POM para simplificar sua configuração do Maven
. Configurar automaticamente o Spring sempre que possível
. Fornecer recursos prontos para produção, como métricas, verificações de integridade e configuração externalizada
. Absolutamente nenhuma geração de código e nenhum requisito para configuração XML

Crie um novo projeto para o Spring Starter
A. Inicie o Eclipse e vá para Arquivo -> Novo -> Outro ... ou pressione Ctrl + N no seu teclado

No campo de pesquisa digite “spring”. Isto irá listar várias opções de Primavera. O que precisamos para este tutorial é o "Spring Starter Project". Selecione e confirme com o botão "Next"

![img-dojo-training-001.jpg](images/img-dojo-training-001.jpg)

*Criando novo projeto inicial da Spring*

B. Use a configuração mostrada na captura de tela abaixo.

Escolha um nome para o seu projeto. Selecione Maven como ferramenta de construção, empacotamento JAR e sua versão Java.

Em Pacote, digite o nome do pacote. Artefato é o nome do arquivo JAR que você irá construir. Por exemplo, se você usar springexample como artefato, o arquivo JAR final será chamado springexample.jar

![img-dojo-training-002.jpg](images/img-dojo-training-002.jpg)

*Configurações iniciais do projeto do Spring Starter*

C. Na janela “Dependências do projeto New Spring Starter”, selecione Web.

Se você não conseguir encontrá-lo na lista, use o campo de pesquisa e digite "web"

![img-dojo-training-003.jpg](images/img-dojo-training-003.jpg)

*Criando um projeto da Web do Spring*

Confirme com o botão “Finish”. O STS criará o projeto para você e baixará todas as dependências necessárias.

D. Uma vez que o projeto é criado, você verá uma classe Java principal chamada SpringBootExampleApplication.java, uma classe Java para fins de teste, um arquivo de propriedades vazio, um arquivo Maven POM e dois arquivos para executar o aplicativo a partir da linha de comando. Na imagem abaixo, você verá à esquerda a estrutura do projeto e o conteúdo da classe principal à direita.

![img-dojo-training-004.jpg](images/img-dojo-training-004.jpg)

*Estrutura do projeto web Spring Boot*

Execute seu aplicativo de inicialização do Spring
Clique com o botão direito do mouse no seu projeto, vá para “Executar como” e selecione “Spring Boot App”

![img-dojo-training-005.jpg](images/img-dojo-training-005.jpg)

*Executar projeto como aplicativo de inicialização Spring*

Isso fará o bootstrap do servidor Tomcat incorporado, implantará seu aplicativo e mapeará os URLs. No console do Eclipse, você verá uma saída semelhante a esta:

```
  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v1.5.8.RELEASE)

2017-11-28 11:15:29.068  INFO 3428 --- [           main] n.j.t.SpringBootExampleApplication       : Starting SpringBootExampleApplication on Filips-MacBook-Air.local with PID 3428 (/Users/filip/Development/GitHub/JavaTutorialNet/SpringBootExample/target/classes started by filip in /Users/filip/Development/GitHub/JavaTutorialNet/SpringBootExample)
2017-11-28 11:15:29.081  INFO 3428 --- [           main] n.j.t.SpringBootExampleApplication       : No active profile set, falling back to default profiles: default
2017-11-28 11:15:29.163  INFO 3428 --- [           main] ationConfigEmbeddedWebApplicationContext : Refreshing org.springframework.boot.context.embedded.AnnotationConfigEmbeddedWebApplicationContext@4461c7e3: startup date [Tue Nov 28 11:15:29 EET 2017]; root of context hierarchy
2017-11-28 11:15:30.753  INFO 3428 --- [           main] s.b.c.e.t.TomcatEmbeddedServletContainer : Tomcat initialized with port(s): 8080 (http)
2017-11-28 11:15:30.773  INFO 3428 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2017-11-28 11:15:30.775  INFO 3428 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet Engine: Apache Tomcat/8.5.23
2017-11-28 11:15:30.949  INFO 3428 --- [ost-startStop-1] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2017-11-28 11:15:30.949  INFO 3428 --- [ost-startStop-1] o.s.web.context.ContextLoader            : Root WebApplicationContext: initialization completed in 1791 ms
2017-11-28 11:15:31.201  INFO 3428 --- [ost-startStop-1] o.s.b.w.servlet.ServletRegistrationBean  : Mapping servlet: 'dispatcherServlet' to [/]
2017-11-28 11:15:31.206  INFO 3428 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'characterEncodingFilter' to: [/*]
2017-11-28 11:15:31.207  INFO 3428 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'hiddenHttpMethodFilter' to: [/*]
2017-11-28 11:15:31.208  INFO 3428 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'httpPutFormContentFilter' to: [/*]
2017-11-28 11:15:31.208  INFO 3428 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'requestContextFilter' to: [/*]
2017-11-28 11:15:31.684  INFO 3428 --- [           main] s.w.s.m.m.a.RequestMappingHandlerAdapter : Looking for @ControllerAdvice: org.springframework.boot.context.embedded.AnnotationConfigEmbeddedWebApplicationContext@4461c7e3: startup date [Tue Nov 28 11:15:29 EET 2017]; root of context hierarchy
2017-11-28 11:15:31.777  INFO 3428 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/error]}" onto public org.springframework.http.ResponseEntity<java.util.Map<java.lang.String, java.lang.Object>> org.springframework.boot.autoconfigure.web.BasicErrorController.error(javax.servlet.http.HttpServletRequest)
2017-11-28 11:15:31.779  INFO 3428 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/error],produces=[text/html]}" onto public org.springframework.web.servlet.ModelAndView org.springframework.boot.autoconfigure.web.BasicErrorController.errorHtml(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)
2017-11-28 11:15:31.820  INFO 3428 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/webjars/**] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2017-11-28 11:15:31.820  INFO 3428 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/**] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2017-11-28 11:15:31.871  INFO 3428 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/**/favicon.ico] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2017-11-28 11:15:32.144  INFO 3428 --- [           main] o.s.j.e.a.AnnotationMBeanExporter        : Registering beans for JMX exposure on startup
2017-11-28 11:15:32.254  INFO 3428 --- [           main] s.b.c.e.t.TomcatEmbeddedServletContainer : Tomcat started on port(s): 8080 (http)
2017-11-28 11:15:32.271  INFO 3428 --- [           main] n.j.t.SpringBootExampleApplication       : Started SpringBootExampleApplication in 13.607 seconds (JVM running for 19.742)

```

Depois que o aplicativo for iniciado, abra seu navegador e navegue para http://localhost:8080

Você verá uma página chamada "Página de erro do Whitelabel". Isso é totalmente legal. Não se preocupe. Indica que o servidor foi iniciado, mas não há mapeamento disponível para URL “/”. Vamos consertar isso na próxima etapa.

![img-dojo-training-006.jpg](images/img-dojo-training-006.jpg)

*Página de erro do whitelabel de primavera*

Crie um mapeamento de solicitações
Agora, para poder ver algum conteúdo "significativo" em http://localhost:8080, precisamos alterar o código de SpringBootExampleApplication.java um lance. Primeiro anote a classe com a anotação @Controller. Segundo, crie um mapeamento de solicitação para o URL "/" - isso mapeará o método home () para esse URL. Em outras palavras, quando o navegador faz uma solicitação GET para http: // localhost: 8080, ele será exibido por esse método. Finalmente, para poder retornar uma resposta ao navegador, é necessário anotar o método home () com @ResponseBody. Você encontrará todas as alterações abaixo:

```
package net.javavatutorial.tutorials;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@SpringBootApplication
public class SpringBootExampleApplication {
	
	@RequestMapping("/")
	@ResponseBody
	String home() {
		return "Greetings from Java Tutorial Network";
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringBootExampleApplication.class, args);
	}
}
```

Relançar o aplicativo Spring Boot
Antes de poder ver as alterações que você acabou de fazer na etapa anterior, é necessário reimplantar seu aplicativo alterado. Para conseguir isso, use o "botão de relançamento da mola" mostrado na imagem abaixo

![img-dojo-training-007.jpg](images/img-dojo-training-007.jpg)

*Botão de relançamento*

Agora vá para o seu navegador e atualize a página. Você deve ver a string que nosso método home () retorna na tela

![img-dojo-training-008.jpg](images/img-dojo-training-008.jpg)

*Saída do programa*

*IntelliJ*

IntelliJ IDEA é um ambiente de desenvolvimento integrado Java para desenvolvimento de software de computador. Ele é desenvolvido pela JetBrains e está disponível como uma edição comunitária do Apache 2 Licensed e em uma edição comercial proprietária. Ambos podem ser usados ​​para desenvolvimento comercial.

Instalação

Para fazer o download do IntelliJ basta acessar o site no link https://www.jetbrains.com/idea/download/ e seguir os procedimentos após o download para instalação.


A. Crie um projeto de inicialização com o IntelliJ
Para iniciar o processo, abra o IntelliJ IDEA e clique na nova opção do projeto.

![img-dojo-training-009.png](images/img-dojo-training-009.png)

*Inicialização do IntelliJ IDE*

B. Selecione Spring Initializr no tipo de projeto no painel do lado esquerdo. Selecione o JDK correto na lista suspensa SDK do Project (o Spring Boot 2.x exige um mínimo de JDK 8).

![img-dojo-training-010.png](images/img-dojo-training-010.png)

C. Insira as propriedades do projeto Maven de acordo com os requisitos do projeto e clique na próxima etapa.

![img-dojo-training-011.png](images/img-dojo-training-011.png)

D. Selecione a versão do Spring Boot e outras dependências necessárias para o seu projeto. Com base nas dependências selecionadas, ele adicionará os iniciadores de inicialização de mola corretos no arquivo pom.xml.

![img-dojo-training-012.png](images/img-dojo-training-012.png)

E. Na última parte do assistente, precisamos selecionar um nome de projeto e um local de projeto. Uma vez selecionado, clique no botão “Finish”.

![img-dojo-training-013.png](images/img-dojo-training-013.png)

Uma vez terminado, o IntelliJ IDEA importa todas as dependências necessárias e abre o projeto recém-criado para trabalhar.

![img-dojo-training-014.png](images/img-dojo-training-014.png)

F. O arquivo pom.xml contém todos os blocos de construção para o aplicativo Spring Boot. Abra o arquivo pom.xml localizado na raiz do módulo do projeto.

```
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
   <modelVersion>4.0.0</modelVersion>
   <groupId>com.javadevjournal</groupId>
   <artifactId>demoproject</artifactId>
   <version>0.0.1-SNAPSHOT</version>
   <packaging>jar</packaging>
   <name>sample-project-IntelliJ</name>
   <description>How to create Spring Boot project using IntelliJ</description>
   <parent>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-parent</artifactId>
      <version>2.0.1.RELEASE</version>
      <relativePath />
      <!-- lookup parent from repository -->
   </parent>
   <properties>
      <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
      <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
      <java.version>1.8</java.version>
   </properties>
   <dependencies>
      <dependency>
         <groupId>org.springframework.boot</groupId>
         <artifactId>spring-boot-starter-thymeleaf</artifactId>
      </dependency>
      <dependency>
         <groupId>org.springframework.boot</groupId>
         <artifactId>spring-boot-starter-web</artifactId>
      </dependency>
      <dependency>
         <groupId>org.springframework.boot</groupId>
         <artifactId>spring-boot-starter-test</artifactId>
         <scope>test</scope>
      </dependency>
   </dependencies>
   <build>
      <plugins>
         <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
         </plugin>
      </plugins>
   </build>
</project>
```

Adicionamos spring-boot-starter-parent como o pai de nosso projeto Spring Boot. Este pai fornece vários recursos no aplicativo Spring Boot

. Configuração - Versão Java e Outras Propriedades.
. Gerenciamento de Dependências - Versão de dependências
. Configuração Padrão de Plugin

G. Classe de Aplicação Principal do Spring Boot
Quando criamos este projeto, o Spring Initializr criou a seguinte classe automaticamente para iniciar nosso aplicativo Spring Boot.

```
package com.javadevjournal.demoproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleProjectIntelliJApplication {
   public static void main(String[] args) {
        SpringApplication.run(SampleProjectIntelliJApplication.class, args);
  }
}
```

H. Executar aplicativo
A classe principal do Spring Boot contém o método main. Este é apenas um método padrão que segue a convenção Java para um ponto de entrada de aplicativo. Nosso método principal delega a SpringApplication classe Spring Boot  chamando  run. SpringApplication bootstraps nossa aplicação, começando a primavera. Podemos executar nosso aplicativo a partir do IntelliJ clicando no ícone do aplicativo de execução

![img-dojo-training-015.png](images/img-dojo-training-015.png)

Neste pequeno documento, abordamos a criação de  um aplicativo Spring Boot usando o IntelliJ . O IntelliJ é realmente um editor poderoso e fornece suporte de primeira classe para criar e executar aplicativos da Web baseados no Spring Boot.



Fontes:

wikipedia.org - https://pt.wikipedia.org/  
javatutorial.net - https://javatutorial.net  
javadevjournal.com - https://www.javadevjournal.com/  

Links:

https://pt.wikipedia.org/wiki/Ambiente_de_desenvolvimento_integrado
https://javatutorial.net/spring-web-app-sts  
https://www.javadevjournal.com/spring-boot/spring-boot-application-intellij/  