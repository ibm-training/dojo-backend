# DOJO - Backend

*DOJO IBM - Treinamento básico para programadores backend*

**1. Sobre o Java**

**Resumo**

Java é uma linguagem de programação orientada a objetos desenvolvida na década de 90 por uma equipe de programadores chefiada por James Gosling, na empresa Sun Microsystems. Em 2008 o Java foi adquirido pela empresa Oracle Corporation. Diferente das linguagens de programação modernas, que são compiladas para código nativo, a linguagem Java é compilada para um bytecode que é interpretado por uma máquina virtual (Java Virtual Machine, mais conhecida pela sua abreviação JVM). A linguagem de programação Java é a linguagem convencional da Plataforma Java, mas não é a sua única linguagem. J2ME Para programas e jogos de computador, celular, calculadoras, ou até mesmo o rádio do carro.

**Prática:**

Instalação

Para instalar o JDK no Linux ou no Windows primeiramente é necessário efetuar o download do arquivo de instalação. Para isso deve-se acessar o site da Oracle (vide seção Links) e baixar versão do JDK correspondente ao sistema operacional e arquitetura (32 ou 64 bits) utilizada.

Após o download do arquivo de instalação ter sido concluído, a instalação do JDK é feita executando-se o programa de instalação. O nome do arquivo de instalação para o Windows tem o formato jdk-<versão>-windows-<arquitetura>.exe, onde <versão> é a versão do JDK e arquitetura pode ser 32 ou 64 bits, por exemplo, jdk-8u66-windows-x64.exe. No sistema operacional Linux o formato do nome é jdk-<versão>-linux-<arquitetura>.tar.gz, por exemplo, jdk-8u66-linux-x64.tar.gz.

Download 

É recomendável que você desative o firewall da Internet antes de continuar com a instalação on-line. Em alguns casos, as configurações padrão do firewall estão definidas para rejeitar instalações automáticas ou on-line, como a do Java. Se o firewall não estiver configurado corretamente, talvez ele atrase a operação de download/instalação do Java sob certas condições. Consulte o seu manual específico para obter instruções sobre como desativar seu firewall da Internet.

Instalação Windows

Para executar o instalador, clique em Executar.

Para salvar o arquivo para instalação posterior, clique em Salvar. 
Selecione a localização da pasta e salve o arquivo no sistema local. 
Dica: salve o arquivo em um local do computador que seja fácil de acessar, como a área de trabalho.
Para iniciar o processo de instalação, clique duas vezes no arquivo salvo.
O processo de instalação será iniciado. Clique no botão Instalar para aceitar os termos de licença e continuar com a instalação. 

![img-dojo-training-023.jpg](images/img-dojo-training-023.jpg)

A Oracle é parceira de algumas empresas que oferecem diversos produtos. O instalador poderá apresentar uma opção para instalar esses programas quando você instalar o Java. Depois de verificar se os programas desejados foram selecionados, clique no botão Avançar para continuar com a instalação.

Algumas caixas de diálogo confirmam as últimas etapas do processo de instalação. Clique em Fechar na última caixa de diálogo. Isso concluirá o processo de instalação do Java. 

![img-dojo-training-024.jpg](images/img-dojo-training-024.jpg)

Listagem 1. Instalação do JDK no Linux

```
- Estando no modo gráfico, abrir um Terminal
    
  - Copiar o arquivo de instalação para o diretório de sua preferência com o comando abaixo
    
    $ cp jdk-<versão>.tar.gz <caminho-completo-do-seu-diretório>
    
  - Mudar para o diretório de sua preferência
    
    $ cd /<caminho-completo-do-seu-diretório>
    
  - Descompactar o arquivo
    
    $ tar -zvxf jdk-<versão>.tar.gz
```

Outra maneira de instalar o JDK no Linux é usando o programa apt-get presente nas distribuições Linux. O apt-get é um programa que permite a instalação e a atualização de pacotes (programas e bibliotecas) de maneira simples e fácil. A Listagem 2 mostra como instalar o JDK usando o apt-get.

Listagem 2. Instalação do JDK no Linux usando apt-get

```
- Estando no modo gráfico, abrir um Terminal e digitar os comandos abaixo
    
    $ sudo add-apt-repository ppa:webupd8team/java
    $ sudo apt-get update
    $ sudo apt-get install oracle-java8-installer
```

O comando add-apt-repository adiciona o local ppa:webupd8team/java no repositório do apt-get. Em seguida o repositório é atualizado com o comando apt-get update. O comando apt-get install irá instalar o pacote JDK 8 da Oracle.

Configuração básica
A configuração básica do JDK consiste na criação das variáveis de ambiente JAVA_HOME e CLASSPATH. Estas variáveis são importantes para que os programas relacionados ao desenvolvimento de aplicações Java possam encontrar, no diretório onde o JDK foi instalado, as diversas bibliotecas para a construção de softwares Java.

Para criar as varáveis JAVA_HOME e CLASSPATH no Linux, executar os comandos da Listagem 3.

Listagem 3. Criando variáveis de ambiente JAVA_HOME e CLASSPATH no Linux

```
- Estando no modo gráfico, abrir um Terminal
    
  - Editar o arquivo /etc/profile com o comando abaixo
    
    $ sudo gedit /etc/profile
    
  - Adicionar as linhas abaixo no inicio do arquivo /etc/profile
    
    JAVA_HOME=diretório-onde-jdk-foi-instalado
    CLASSPATH=.;$JAVA_HOME/lib
    PATH=$PATH:$JAVA_HOME/bin
    export JAVA_HOME
    export CLASSPATH
    export PATH
    
  - Salvar o arquivo e efetuar um logoff para que as variáveis de ambiente entrem em vigor.
```

O arquivo /etc/profile mencionado na Listagem 3 é um arquivo padrão dos sistemas operacionais Linux e ele armazena configurações básicas de inicialização do perfil do usuário.

No Windows as variáveis de ambiente são criadas conforme a Listagem 4.

Listagem 4. Criando variáveis de ambiente JAVA_HOME e CLASSPATH no Windows

```
- Abrir um Prompt do MS-DOS
    
  - Executar os comandos abaixo
    
    setx JAVA_HOME "<diretório-onde-jdk-foi-instalado>"
    setx CLASSPATH %JAVA_HOME%\lib
    setx PATH %PATH%;%JAVA_HOME%\bin
    
  - Fechar o Prompt do MS-DOS
```

O comando setx.exe é parte do Windows Vista ou superior. Versões anteriores precisam de um pacote adicional chamado Windows Resource Kit. O detalhamento deste pacote adicional esta fora do escopo desse artigo.

Testando a configuração básica
Para testar a configuração das variáveis de ambiente, tanto no Linux como no Windows, executar os comandos da Listagem 5.

Listagem 5. Testando a configuração básica no Linux e no Windows

````
- Abrir um Terminal (Linux) ou um Prompt do MS-DOS (Windows)
    
  - Digitar o comando abaixo
    
    javac -version
````

O comando javac -version deverá produzir uma saída no console do Terminal Linux ou do Prompt MS-DOS contendo a versão do Java. Caso isso não aconteça, verifique se a versão correspondente ao sistema operacional foi instalada corretamente (revendo os passos das seções Instalação e Configuração básica), se o diretório foi informado corretamente na variável de ambiente JAVA_HOME e se o logoff foi efetuado.

**Mais sobre o Java**

Para mais informações sobre a linguagem Java recomendamos a leitura sobre os fundamentos no seguintes links abaixo:

Parte 1  
Noções básicas da linguagem Java - Programação orientada a objetos na plataforma Java  
Link Conteúdo (Em inglês): https://developer.ibm.com/tutorials/j-introtojava1/  

Parte 2  
Construções para aplicativos do mundo real - Recursos de linguagem Java mais avançados  
Link Conteúdo (Em inglês):  https://developer.ibm.com/tutorials/j-introtojava2/  

Adicionais  
Unidade 10: Coleções Java - Crie e gerencie coleções de objetos  
https://developer.ibm.com/tutorials/j-perry-java-collections/  

Fontes:

developer.ibm.com - https://developer.ibm.com/  
wikipedia.org - https://pt.wikipedia.org/  
tutorialsPoint.com - https://www.tutorialspoint.com/  
devmedia.com.br - https://www.devmedia.com.br  

Links:

https://pt.wikipedia.org/wiki/Java_(linguagem_de_programação)  
https://www.tutorialspoint.com/java/index.htm  
https://developer.ibm.com/tutorials/j-introtojava1/  
https://developer.ibm.com/tutorials/j-introtojava2/  
https://www.devmedia.com.br/instalacao-e-configuracao-do-pacote-java-jdk/23749  