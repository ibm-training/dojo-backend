# DOJO - Backend

*DOJO IBM - Treinamento básico para programadores backend*

**3. Maven**

**Resumo**

O que é o Maven?

O Maven é uma poderosa ferramenta de gerenciamento de projetos baseada no POM (project object model). É usado para criação, dependência e documentação de projetos. Simplifica o processo de construção como ANT. Mas é muito avançado do que ANT. 

Em suma, podemos dizer que maven é uma ferramenta que pode ser usada para construir e gerenciar qualquer projeto baseado em Java. O maven facilita o trabalho do dia-a-dia dos desenvolvedores Java e geralmente ajuda na compreensão de qualquer projeto baseado em Java.

**Prática:**

Instalando o Maven

Windows

Para começar, faça o download do Maven (arquivo .zip, exemplo: apache-maven-3.5.0-bin.zip) no site oficial http://maven.apache.org/download.cgi:

![img-dojo-training-027.png](images/img-dojo-training-027.png)

Depois, descompacta este arquivo zip na pasta: C:\Program Files\Apache\maven. Ficando assim:

![img-dojo-training-028.png](images/img-dojo-training-028.png)

Será necessário criar essas pastas, caso não tenha elas no Program Files.

Pronto! O Maven esta instalado, agora será necessário configura-lo!

Configurando o JDK

Para esse etapa, será necessário que o JDK mais atual esteja instalado!

Inicie configurando a variável de ambiente do Windows chamada “JAVA_HOME“, com o valor do caminho do JDK instalado na sua máquina. No meu ficou com o caminho “C:\Program Files\Java\jdk1.8.0_144“:

Acesse a pasta “Seu Computador”, clique com botão direito e depois em propriedade:

![img-dojo-training-029.png](images/img-dojo-training-029.png)

Depois, clique em “Configurações avançadas do sistema”:

![img-dojo-training-030.png](images/img-dojo-training-030.png)

Clique no botão “Variáveis de Ambiente”:

![img-dojo-training-031.png](images/img-dojo-training-031.png)

Abrindo a janela abaixo, você consegue configurar apenas no usuário logado ou para o sistema (todos os usuários). No meu caso, escolhi configurar para o sistema:

![img-dojo-training-032.png](images/img-dojo-training-032.png)

Depois, é só colocar o nome o diretório:

![img-dojo-training-033.png](images/img-dojo-training-033.png)

E pronto!

Configurando o Maven

Para configurar o Maven, não tem segredo! Vamos adicionar mais duas variáveis de ambiente a M2_HOME and MAVEN_HOME, as duas com o caminho da pasta do Maven que descompactamos “C:\Program Files\Apache\maven“:

![img-dojo-training-034.png](images/img-dojo-training-034.png)

E por último, vamos adicionar a variável “Path” o caminho dos binários do Maven, com “%M2_HOME%\bin;”, desta forma:

![img-dojo-training-035.png](images/img-dojo-training-035.png)

E adicione:

![img-dojo-training-036.png](images/img-dojo-training-036.png)

Só confirmar todas as configurações, clicando em “OK” em todos as janelas abertas e pronto!

Verificando a instalação do Maven

Para confirmar se a instalação foi feito com sucesso, basta abrir o prompt de comando do Windows e digitar “mvn-version“, deve mostrar:

![img-dojo-training-037.png](images/img-dojo-training-037.png)

Linux

A primeira coisa a se fazer é baixar a distribuição que iremos instalar. No terminal, digite o comando abaixo para realizar o download.

```
curl -OL http://ftp.unicamp.br/pub/apache/maven/maven-3/3.5.3/binaries/apache-maven-3.5.3-bin.tar.gz
```

Em seguida, precisamos descompactar o arquivo. O local da nossa instalação ficará em /usr/local/. Aqui você entende o porquê.

```
sudo tar -zxf ./apache-maven-3.5.3-bin.tar.gz -C /usr/local/
```

Na sequência, criaremos um link simbólico para o diretório de instalação para que essa instância esteja visível para todos os usuários.

```
sudo ln -s /usr/local/apache-maven-3.5.3/bin/mvn /usr/bin/mvn
```

Finalizando Permalink
Feito isso, seja usando apt ou baixando diretamente o executável, nossa instalação está pronta! Para testar, digite mvn -v no terminal e você terá algo como:

```
Apache Maven 3.5.3
Maven home: /usr/local/apache-maven-3.5.3
Java version: 1.8.0_60, vendor: Oracle Corporation
Java home: /usr/lib/jvm/java-8-oracle/jre
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "3.13.0-62-generic", arch: "amd64", family: "unix"
```

O que maven faz?

Maven faz um monte de tarefas úteis como

- Podemos facilmente construir um projeto usando o maven.
- Podemos adicionar frascos e outras dependências do projeto facilmente usando a ajuda do maven.
- O Maven fornece informações do projeto (documento de log, lista de dependências, relatórios de teste de unidade etc.)
- O Maven é muito útil para um projeto enquanto atualiza o repositório central de JARs e outras dependências.
- Com a ajuda do Maven, podemos construir qualquer número de projetos em tipos de saída, como o JAR, WAR etc, sem fazer qualquer script.
- Usando o maven, podemos integrar facilmente nosso projeto ao sistema de controle de origem (como o Subversion ou o Git).

Como funciona o maven?

![img-dojo-training-025.jpg](images/img-dojo-training-025.jpg)

Conceitos Básicos do Maven:

Arquivos POM: Os arquivos do Modelo de Objeto de Projeto (POM) são arquivos XML que contêm informações relacionadas ao projeto e informações de configuração, como dependências, diretório de origem, plug-in, metas etc. usadas pelo Maven para construir o projeto. Quando você deve executar um comando maven, dê ao maven um arquivo POM para executar os comandos. O Maven lê o arquivo pom.xml para realizar suas configurações e operações.

Dependências e Repositórios: Dependências são bibliotecas Java externas necessárias para o Projeto e repositórios são diretórios de arquivos JAR compactados. O repositório local é apenas um diretório no disco rígido da sua máquina. Se as dependências não forem encontradas no repositório local do Maven, o Maven fará o download delas a partir de um repositório central do Maven e as colocará no seu repositório local.

Construir Ciclos de Vida, Fases e Metas: Um ciclo de vida de construção consiste em uma sequência de fases de construção, e cada fase de construção consiste em uma sequência de objetivos. Comando Maven é o nome de um ciclo de vida de construção, fase ou meta. Se um ciclo de vida for solicitado executando o comando maven, todas as fases de construção nesse ciclo de vida também serão executadas. Se uma fase de construção é solicitada executada, todas as fases de construção antes dela na sequência definida também são executadas.

Construir Perfis: Construir perfis um conjunto de valores de configuração que permite construir seu projeto usando diferentes configurações. Por exemplo, você pode precisar construir seu projeto para seu computador local, para desenvolvimento e teste. Para habilitar diferentes construções, você pode adicionar diferentes perfis de construção aos seus arquivos POM usando seus elementos de perfis e são acionados da variedade de maneiras.

Plugins de compilação: Plugins de compilação são usados ​​para executar metas específicas. você pode adicionar um plugin ao arquivo POM. O Maven tem alguns plugins padrão que você pode usar, e você também pode implementar o seu próprio em Java.

Processo de instalação do Maven

A instalação do Maven inclui os seguintes passos:

- Verifique se o seu sistema tem o java instalado ou não. caso contrário, instale o java.
- Verificar java a variável ambiental está definida ou não. se não, então, defina a variável ambiental java.
- Baixar o Maven 
- Desembale seu zip maven em qualquer lugar em seu sistema.
- Adicione o diretório bin do diretório criado apache-maven-3.5.3 (depende da sua versão de instalação) para a variável de ambiente PATH e variável de sistema.
- Abra cmd e execute o comando mvn -v . Se imprimir as seguintes linhas de código, a instalação será concluída.

````
Apache Maven 3.5.3 (3383c37e1f9e9b3bc3df5050c29c8aff9f295297; 2018-02-25T01:19:05+05:30)
Maven home: C:\apache-maven-3.5.3\bin\..
Java version: 1.8.0_151, vendor: Oracle Corporation
Java home: C:\Program Files\Java\jdk1.8.0_151\jre
Default locale: en_US, platform encoding: Cp1252
OS name: "windows 10", version: "10.0", arch: "amd64", family: "windows"
````

Arquivo Maven pom.xml

POM significa que o Modelo de Objeto do Projeto é a chave para operar o Maven. O Maven lê o arquivo pom.xml para realizar suas configurações e operações. Ele é um arquivo XML que contém informações relacionadas com as informações do projeto e de configuração, como dependências , diretório de origem , plug-in , metas etc . usado pelo Maven para construir o projeto.

A amostra de pom.xml

```
<project xmlns="http://maven.apache.org/POM/4.0.0" 
   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 
http://maven.apache.org/xsd/maven-4.0.0.xsd"> 
          
         <modelVersion>4.0.0</modelVersion> 
         <groupId> com.project.loggerapi </groupId> 
         <artifactId>LoggerApi</artifactId> 
         <version>0.0.1-SNAPSHOT</version> 
           
       <!-- Add typical dependencies for a web application -->
       <dependencies> 
               <dependency> 
                       <groupId>org.apache.logging.log4j</groupId> 
                       <artifactId>log4j-api</artifactId> 
                       <version>2.11.0</version> 
                 </dependency> 
       </dependencies> 
     
   </project> 
```

Elementos usados ​​para criar o arquivo pom.xml

- project- É o elemento raiz do arquivo pom.xml.
- modelVersion- modelversion significa qual versão do modelo POM você está usando. Use a versão 4.0.0 para maven 2 e maven 3.
- groupId- groupId significa o id para o grupo de projetos. Ele é único e, na maioria das vezes, você usa um ID de grupo que é semelhante ao nome do pacote raiz do projeto Java, como usamos o groupId com.project.loggerapi.
- artifactId- artifactId usado para fornecer o nome do projeto que você está construindo.Nosso nome de exemplo do nosso projeto é LoggerApi.
- Versão elemento versão contém o número da versão do projeto. Se o seu projeto foi lançado em versões diferentes, então é útil dar versão do seu projeto.

Outros elementos do arquivo Pom.xml

- O elemento dependencies- dependencies é usado para definir uma lista de dependências do projeto.
- dependency dependency define uma dependência e usa uma tag de dependências interna. Cada dependência é descrita pelo seu groupId, artifactId e version.
- nome - este elemento é usado para dar nome ao nosso projeto maven.
- scope- este elemento usado para definir o escopo deste projeto de maven que pode ser compilado, tempo de execução, teste, sistema fornecido, etc.
- O elemento packaging- packaging é usado para empacotar nosso projeto para tipos de saída como JAR, WAR etc.

Repositório Maven
Os repositórios Maven são diretórios de arquivos JAR compactados com alguns metadados. Os metadados são arquivos POM relacionados aos projetos a que cada arquivo JAR empacotado pertence, incluindo quais dependências externas cada JAR empacotado possui. Esses metadados permitem que o Maven baixe as dependências de suas dependências de forma recursiva até que todas as dependências sejam baixadas e colocadas em sua máquina local.

O Maven possui três tipos de repositório:

- Repositório local
- Central repository
- Repositório Remoto

O Maven procura por dependências neste repositório. Primeiro maven procura no repositório local, em seguida, repositório Central, em seguida, repositório remoto se o repositório remoto especificado no POM.

![img-dojo-training-026.jpg](images/img-dojo-training-026.jpg)

1. Repositório local - Um repositório local é um diretório na máquina do desenvolvedor. Este repositório contém todas as dependências que o Maven transfere. O Maven só precisa baixar as dependências uma vez, mesmo que vários projetos dependam delas (por exemplo, ODBC). 
Por padrão, o repositório local do maven é o diretório user_home / m2. 
exemplo - C: \ Users \ asingh \ .m2

2. Repositório Central - O repositório central do Maven é criado pela comunidade Maven. O Maven procura neste repositório central por qualquer dependência necessária, mas não encontrada em seu repositório local. O Maven faz o download dessas dependências em seu repositório local. Você pode ver o repositório central https://search.maven.org/.

3. Repositório remoto - repositório remoto é um repositório em um servidor web do qual o Maven pode baixar dependencies.it frequentemente usado para hospedar projetos internos da organização. O Maven faz o download dessas dependências em seu repositório local.

Aplicação Prática Do Maven

Ao trabalhar em um projeto java e esse projeto contém muitas dependências, compilações, requisitos, então manipular todas essas coisas manualmente é muito difícil e cansativo. Assim, usando alguma ferramenta que pode fazer essas obras é muito útil. 
O Maven é uma ferramenta de gerenciamento de build que pode fazer todas as coisas, como adicionar dependências, gerenciar o classpath para projetar, gerar arquivos war e jar automaticamente e muitas outras coisas.

Prós e contras do uso do Maven
Prós:

- O Maven pode adicionar automaticamente todas as dependências necessárias para o projeto, lendo o arquivo pom.
- Pode-se facilmente construir seu projeto para jar, war etc., conforme seus requisitos usando Maven.
- O Maven facilita o início do projeto em diferentes ambientes e não é necessário manipular a injeção de dependências, construções, processamento, etc.
- Adicionando uma nova dependência é muito fácil. É preciso apenas escrever o código de dependência no arquivo pom.

Contras:

- Maven precisa da instalação do maven no sistema para o plugin de trabalho e maven para o ide.
- Se o código do maven para uma dependência existente não estiver disponível, não será possível incluir essa dependência usando maven.

Quando alguém deveria usar o Maven?

Pode-se usar o Maven Build Tool na seguinte condição:

1. Quando há muitas dependências para o projeto. Então é fácil lidar com essas dependências usando maven.  
2. Quando a versão de dependência é atualizada com freqüência. Então, é necessário atualizar apenas o ID da versão no arquivo pom para atualizar as dependências.  
3. Construções, integrações e testes contínuos podem ser facilmente manipulados usando maven.  
4. Quando se precisa de uma maneira fácil de gerar documentação a partir do código-fonte, Compilando o código-fonte, Compactando o código compilado em arquivos JAR ou arquivos ZIP.  


Fontes:

geeksforgeeks.org - https://www.geeksforgeeks.org  
maven.apache.org - https://maven.apache.org/  

Links:

https://maven.apache.org/guides/getting-started/index.html 
https://www.geeksforgeeks.org/introduction-apache-maven-build-automation-tool-java-projects/  